import yaml
import os


class StringData:

    def __init__(self, data, key):
        def locate():
            for sigil in ['p', 'q', 'x']:
                skey = key + '+' + sigil
                if skey in data:
                    return sigil, data[skey]
            if key in data:
                return 'p', data[key]
            else:
                raise KeyError(key)
        self.key = key
        self.qtype, self.value = locate()

    @property
    def oneline(self):
        if len(self.lines) == 0:
            return ''
        if len(self.lines) == 1:
            return self.lines[0]
        return '%s[...]' % self.lines[0]

    def __str__(self):
        return self.oneline

    @property
    def lines(self):
        return self.value.splitlines()


class DurationData:

    @classmethod
    def from_eventdata(cls, data):
        try:
            duration = int(data['data'].get('r.duration', ''))
        except ValueError:
            duration = None
        return cls(duration)

    @classmethod
    def from_phasedata(cls, data):
        try:
            duration = int(data['phase'].get('duration', ''))
        except ValueError:
            duration = None
        return cls(duration)

    def __init__(self, duration=None):
        self.value = duration

    def __nonzero__(self):
        return self.value is not None

    def __str__(self):
        if self.value is None:
            return '?'
        else:
            return '%d' % self.value


class ParsingError(ValueError):
    pass


class _EventGroup(object):

    def __init__(self, events, run):
        if not events:
            raise ValueError("no events in event group")
        self.run = run
        self._events = events

    @property
    def all_events(self):
        return self._events

    @property
    def asserts(self):
        return [e for e in self._events if e.is_assert]

    @property
    def fails(self):
        return [e for e in self._events if e.etype == 'ASSERT.FAIL']

    @property
    def passes(self):
        return [e for e in self._events if e.etype == 'ASSERT.PASS']

    @property
    def events(self):
        return self._events

    @property
    def lints(self):
        lints = set()
        for e in self._events:
            lints.update(e.lints)
        return lints


class Case(_EventGroup):

    @property
    def id(self):
        caseids = set([e.caseid for e in self._events])
        assert len(caseids) == 1, "case with multiple ids?"
        return caseids.pop()

    @property
    def verdict(self):
        seen = set([e.verdict for e in self._events])
        if seen == set(['PASS']):
            return 'PASS'
        else:
            return 'FAIL'


class EventTree(object):

    def __init__(self, events, run, head=None, tail=None):
        self.children = []
        self.run = run
        self.rest = [e for e in events]
        self.head = head
        self.tail = tail
        self._lints = []
        while self.rest:
            if self.peek().startswith('RUN.'):
                self.pop()
            elif self.peek() == 'PHASE.START':
                phead = self.pop()
                pbuff = self.pull_branch('PHASE.END')
                pbody = pbuff[:-1]
                ptail = pbuff[-1]
                self.children.append(Phase(pbody, run, phead, ptail))
            elif self.peek().startswith('MESSAGE.'):
                self.children.append(self.pop())
            elif self.peek().startswith('RELIC.'):
                self.children.append(self.pop())
            elif self.peek().startswith('ASSERT.'):
                self.children.append(self.pop())
            elif self.peek() == 'PHASE.END':
                stray = self.pop()
                self._lints.append(Lint(
                    'stray phase end',
                    stray._rawdata
                ))
            else:
                raise ParsingError(
                    'unexpected event type when building tree: %s'
                    % self.peek()
                )

    def peek(self):
        return self.rest[0].etype

    def pop(self):
        return self.rest.pop(0)

    def pull_branch(self, final_etype):
        out = []
        while self.rest:
            if self.peek() == final_etype:
                return out + [self.pop()]
            else:
                out.append(self.pop())
        self._lints += [Lint('could not find: %s' % final_etype, out)]
        return out

    @property
    def lints(self):
        lints = set(self._lints)
        for e in self.children:
            lints.update(e.lints)
        return lints


class Phase(EventTree):

    is_phase = True

    @property
    def id(self):
        return self.head._rawdata['phase']['id']

    @property
    def hint(self):
        return self.head._rawdata['phase']['hint']

    @property
    def pretty_hint(self):
        pcaseid = self.head._rawdata['data']['pcaseid']
        ptype = self.head._rawdata['phase']['type']
        phint = self.head._rawdata['phase']['hint']
        out = []
        if pcaseid:
            out.append("[%s]" % pcaseid)
        if phint:
            out.append(phint)
        out.append("# %s" % ptype)
        return " ".join(out)

    @property
    def type(self):
        return self.head._rawdata['phase']['type']

    @property
    def duration(self):
        return DurationData.from_phasedata(self.tail._rawdata)

    @property
    def verdict(self):
        out = ''
        for e in self.children:
            if e.is_assert:
                if e.verdict == 'PASS':
                    out = 'PASS'
                elif e.verdict == 'FAIL':
                    return 'FAIL'
        return out


class Event(object):

    is_assert = False
    is_message = False
    is_relic = False
    is_phase = False

    def __str__(self):
        return str(self._rawdata)

    def __init__(self, rawdata, run):
        self._rawdata = rawdata
        self.etype = rawdata['etype']
        self.stamp = rawdata['stamp']
        self.data = rawdata.get('data', dict())
        self.run = run

    @staticmethod
    def from_data(data, run):
        etype = data['etype']
        if etype in ['ASSERT.PASS', 'ASSERT.FAIL']:
            cls = AssertEvent
        elif etype in ['PHASE.START', 'PHASE.END']:
            cls = StructureInfoEvent
        elif etype in ['RUN.START', 'RUN.RELOAD', 'RUN.END']:
            cls = StructureInfoEvent
        elif etype == 'MESSAGE.INFO':
            cls = MessageEvent
        elif etype == 'MESSAGE.WARNING':
            cls = MessageEvent
        elif etype == 'MESSAGE.ERROR':
            cls = MessageEvent
        elif etype == 'MESSAGE.SKIP':
            cls = MessageEvent
        elif etype == 'MESSAGE.PROMISE':
            cls = PromiseEvent
        elif etype == 'RELIC.DATA':
            cls = RelicEvent
        elif etype == 'RELIC.FILE':
            cls = RelicEvent
        else:
            raise ParsingError("unknown event type: %s" % etype)
        return cls(data, run=run)

    @property
    def lints(self):
        return set()


class MessageEvent(Event):

    is_message = True

    @property
    def message(self):
        return StringData(self._rawdata, 'message')

    @property
    def severity(self):
        return self.etype.split('.')[1]

    @property
    def lints(self):
        if self.severity == 'WARNING':
            return set([Lint("test warning", self._rawdata)])
        elif self.severity == 'ERROR':
            return set([Lint("test error", self._rawdata)])
        else:
            return set()


class RelicEvent(Event):

    is_relic = True

    @property
    def lints(self):
        return set()


class PromiseEvent(Event):

    @property
    def lints(self):
        return set([Lint("promises are not supported (yet)", self._rawdata)])


class StructureInfoEvent(Event):
    pass


class AssertEvent(Event):

    is_assert = True

    @property
    def caseid(self):
        if self._rawdata['caseid']:
            return self._rawdata['caseid']
        return self.run.id

    @property
    def duration(self):
        return DurationData.from_eventdata(self._rawdata)

    @property
    def hint(self):
        return StringData(self._rawdata, 'hint')

    @property
    def verdict(self):
        return self.etype.split('.')[1]


class EventLog(_EventGroup):
    """
    Run event log (all events from run)
    """

    @classmethod
    def from_data(cls, data, run):
        return cls([Event.from_data(e, run=run) for e in data],
                   run=run)

    @property
    def caseids(self):
        return set([a.caseid for a in self.asserts])

    @property
    def cases(self):
        out = []
        for caseid in self.caseids:
            out.append(Case([
                a for a in self.asserts if a.caseid == caseid
            ], self.run))
        return out


class Lint(object):
    """
    Lint-like warning coming from log parsing (eg. unclosed phase)
    """

    def __init__(self, msg, data):
        self.msg = msg
        self._data = data

    def __eq__(self, othr):
        return hash(self) == hash(othr)

    def __hash__(self):
        #FIXME: not really unique or pretty
        return hash(self.msg + str(self._data))


class Run(object):

    def __init__(self, doc):
        self._doc = doc
        self._format = doc['format']
        if not self._format.startswith('jat/0.'):
            raise ParsingError('unsupported log format: %s' % self._format)
        self.jat_version = doc['jat_version']
        self.start = doc['start']
        self.id = doc.get('id', 'noid')
        self.test = doc.get('test')
        self.finalized = doc.get('finalized', False)
        self.end = doc.get('end')
        self.lints = set()
        self.eventlog = EventLog.from_data(doc['events'], run=self)
        self.lints.update(self.eventlog.lints)
        self.cases = self.eventlog.cases
        self.eventtree = EventTree(self.eventlog._events, run=self)
        self.lints.update(self.eventtree.lints)
        self.relics = [e for e in self.eventlog._events if e.is_relic]
        if not self.finalized:
            self.lints.update([Lint("run not finalized", self.id)])


def load(fpath):
    with open(fpath) as ylog:
        runs = [Run(doc) for doc in yaml.safe_load_all(ylog)]
    return runs


def load_last():
    fn = os.environ.get('JAT__DIR', '/var/tmp/jat') + '/last/log.yaml'
    return load(fn)
